Organizing XML Route Definitions
================================

The Camel Spring Boot Starter allows for automatic detection and configuration of XML routes. see: [Adding XML Routes](https://github.com/apache/camel/blob/camel-2.21.x/components/camel-spring-boot/src/main/docs/spring-boot.adoc#adding-xml-routes)

By default, this only looks for files one level deep in the camel/ directory on the classpath. This project demonstrates changing that so you can use a deeper structure to organize route definitions.

The critical change is in application.properties
```properties
camel.springboot.xml-rests = classpath:camel/**/*.xml
```

This allows a project structure like the following
```
├── pom.xml
├── README.md
├── src
│   └── main
│       ├── java
│       │   └── rh
│       │       └── demo
│       │           └── Application.java
│       └── resources
│           ├── application.properties
│           └── camel
│               ├── nested
│               │   ├── deeply-nested
│               │   │   └── deeplynestedroute.xml
│               │   └── nestedroute.xml
│               └── toproute.xml
```
